function[gss,axs] = gaussia (var,mean)
limit1 = mean - 3 * sqrt(var);
limit2 = mean + 3 * sqrt(var);
axs = limit1: abs(limit2 - limit1) / 512: limit2;
gss = (1/(var * sqrt(2*pi))).*exp(-(1/(var*2)) * ((axs-mean).^2));