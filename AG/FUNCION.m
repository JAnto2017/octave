function [temp,ind] = FUNCION(x,d,N)
y=[];
     for j=1:N
          a=20; b=0.2; c=2*pi;
          s1=0; s2=0;
          for i=1:d
               s1=s1+x(j,i)^2;
               s2=s2+cos(c*x(j,i));
          endfor
          y(j,1)=-a*exp(-b*sqrt(1/d*s1))-exp(1/d*s2)+a+exp(1);
     endfor
[temp,ind]=sort(y);