%CONVIERTE DE BINARIO A DECIMAL
function r = DECOB(B,l,u,Nb)
%r: valor real
%B: cadena binaria de Nb bits
%Nb: nº de bit de la cadena
%l: limite inferior del espacio de busqueda
%u: limite superior del espacio de busqueda

     B=fliplr(B);
     t1 = (Nb-1):-1:0;
     t2 = ones(1,Nb);
     t3 = sum((t2.*2.^t1)')+1;
     t2 = sum((B.*2.^t1)');             %Nº real 
     r = 1 + t2 .* ((u - 1) / t3);      %Aplica limites busqueda
     end