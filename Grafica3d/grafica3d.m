%Gráfica en tres dimensiones de z=exp(-x^2-y^2)
xx = -2:.2:2;
yy = xx;
[x,y] = meshgrid(xx,yy);
z=exp(-x.^2-y.^2);
mesh(z);