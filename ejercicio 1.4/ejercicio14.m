%ejercicio 1.4 graficar numeros complejos en forma polar
a1 = -3j;
a2 = 20-10j;
a3 = -20;
a4 = 5+8j;
%theta=-pi/2:pi/64:pi/2;
hold on
polar(angle(a1),abs(a1))
polar(angle(a2),abs(a2))
polar(angle(a3),abs(a3))
polar(angle(a4),abs(a4))