theta = 40;
theta_rad = theta*pi/180;
velocity = 45;
g = 9.8;
%----------------------------------------------------
v0x = velocity * cos(theta_rad);
v0y = velocity * sin(theta_rad);
time = 2*v0y/g;
distance = v0x * time;
fprintf('Time elapsed %12.5f \n',time)
fprintf('Distance travelled by the ball %12.5f \n',distance)
x = 0: 0.1: distance;
y = (v0y / v0x) * x-(1/2) * (g/v0x^2)*x.^2;
plot(x,y)
xlabel('Distance')
ylabel('Height')
grid on