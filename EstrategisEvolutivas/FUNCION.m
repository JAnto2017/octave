%Función objetivo en las estrategias evolutivas EE
function [f,i1] =FUNCION(x,iter,d)
     y=[];
     %Michekewics
     for i1=1:iter
          m=10;
          s=0;
          for i2=1:d
               s = s + sin(x(i1,i2)) * (sin(i2*x(i1,i2)^2/pi))^(2*m);
          end
          y(i1,1)=-s;
      
      end
      [f,i1]=sort(y);
  endfunction    