function diffex(x,n) 
format long
func=-0.1*x^4;
dfunc=-0.4*x^3;
dftrue= dfunc;
h=1;
H(1)=h;
D(1)=(func(x+h)-func(x-h)) / (2*h);
E(1)=abs(dftru-D(1));

for i=2:n
     h=H/10;
     H(i)=h;
     D(i)=(func(x+h)-func(x-h)) / (2*h);
     E(i)=abs(dftrue-D(i));
end

L=[H' D' E']';
fprintf(' step size finite difference true error\n');
fprintf('%14.10f %16.14f %16.13f\n',L);
loglog(H,E) ,xlabel('Step Size') ,ylabel('Error')
title('plot of error versus step size')
format short 