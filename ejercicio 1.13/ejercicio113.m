%Ejercicio 1.13, graficar z = x^2*y utilizando mesh
xx=-21:.2:61;
yy=xx;
[x,y] = meshgrid(xx,yy);
z=x.^2 * y;
mesh(z);
